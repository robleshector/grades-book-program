const express = require('express');
const Record = require('./../models/Records')
const Year = require('./../models/Years')

const router = express.Router();

// POST
router.post('/', (req,res,next) => {

    // Convert input
    const input = req.body.input.toLowerCase().split(" ");

    // Process Record
    function processRecord(input, yearId){
        year = yearId;
        
        // Create Functions and Prototype
        // Average
        function avg(arr) {
            let sum = 0;
            let length = 0;
            arr.forEach(num => {
                if(num) {
                    sum += num
                    length++
                }
            })
            return sum / length
        }
    
        // StudentPrototype
        function Student(name, yearId) {
            this.name = name,
            this.yearId = yearId,
            this.quarterGrades = [0,0,0,0],
            this.finalGrade = 0
        }
    
        // Initialize variables
        let records = [];
        let foundRecord;
        let homeworkScores = [];
        let testScores = [];
        let quarterGrade = 0;
        let isTest;
        let quarter = input[1].replace(",","");
    
        // Process Input
        for (i = 0; i < input.length; i++) {
    
            // Check for a student's name
            if (input[i] != "quarter" && input[i - 1] != "quarter" && input[i] != "h" && input[i] != "t" && isNaN(input[i])) {
    
                // Set student name
                studentName = `${input[i]} ${input[i + 1]}`;
    
                // Look for duplicate record
                foundRecord = records.find(record => {record.name === studentName, record.yearId == year});
                if(!foundRecord) {
                    foundRecord = new Student(studentName, year);
                    records.push(foundRecord);
                }
    
                // Adjust Index
                i += 2;
            }
    
            // Check if Part of Homeworks or Tests
            if (input[i] == "t") {
                isTest = true;
            } else if (input[i] == "h") {
                isTest = false;
            }
    
            // Push Scores 
            if (!isNaN(input[i]) && input[i] <= 100) {
                if (isTest) {
                    testScores.push(Number(input[i]))
                } else {
                    homeworkScores.push(Number(input[i]))
                }
            }
            
            // Check for last number in series of scores
            if (!isNaN(input[i]) && input[i] <= 100 && isNaN(input[i + 1]) && input[i + 1] != "t" && input[i + 1] != "h") {
                // Compute Quarter Grade
                // Remove Lowest-scored Homework
                if(homeworkScores.length > 1) {
                    homeworkScores.sort((x, y) => { return x - y });
                    homeworkScores.splice(0, 1);
                }
    
                // Compute Quarter Grade
                quarterGrade = avg(testScores) * .6 + avg(homeworkScores) * .4;
    
                // Push Quarter Grade
                foundRecord.quarterGrades[quarter-1] = quarterGrade;
    
                // Clear variables
                testScores = [];
                homeworkScores = [];
            }
        }

         let names = [];

         records.forEach( n => {
                    names.push(n.name)
                });

         Record.find({name: names, yearId : yearId})
         .then(data => {
            // Process existing records
            data.forEach( foundRecord => {
                let record = records.find( record => record.name == foundRecord.name)
                if(record) {
                    // Remove existing record from list
                    let index = records.findIndex(record => record.name == foundRecord.name);
                    records.splice(index,1);
                    // records.splice((records.indexOf(record => record.name == foundRecord.name)), 1);

                    for(i=0; i < record.quarterGrades.length; i++) {
                        if(record.quarterGrades[i] != 0) {
                            foundRecord.quarterGrades[i] = record.quarterGrades[i];
                        }
                    }
                    
                    // Compute Final Grade
                    foundRecord.finalGrade = Math.round(avg(foundRecord.quarterGrades) * 10) / 10;

                    // Save Record
                    Record.findByIdAndUpdate(foundRecord.id, foundRecord, {new: true})
                    .then(res)
                    .catch(next)
                }
            })

            if(Array.isArray(data)) {
                return data
            } else {
                return [data]
            }
        })
         .then(data => {
            // Create non-existing records
            if(records[0]) {
                // Compute Final Grade
                records.forEach( record => {
                    record.finalGrade = Math.round(avg(record.quarterGrades) * 10) / 10;
                })
            }

            // Save records
            Record.create(records)
            .then(res => {
                let result = {
                        updated : [...data],
                        created : []
                    }

                if(res !== undefined && Array.isArray(res)) {
                    result.created = res;
                } else if(res !== undefined && !Array.isArray(res)) {
                    result.created = [res];
                }

                return result;
            })
            .then( records => {
                res.send(records);
            })
         })
        }

    
    // START
    let year = input.find(res => {
        return res.length === 4 && !isNaN(res);
    })

    let quarterIndex = input.findIndex(res => res == "quarter");
    let quarterValue = input[quarterIndex+1].replace(",","")

    if(year && quarterIndex >= 0 && quarterValue > 0 && quarterValue < 5) {
        // Check Year
        Year.findOne({name: year})
        .then(foundYear => {
            if(!foundYear) {
                Year.create({
                    name: year
                })
                .then(newYear => {
                    processRecord(input,newYear._id)
                })
            } else {
                processRecord(input,foundYear._id)
            }
        })
        .catch(next)
    } else {
        res.json("Invalid Format, no year found.")
    }
})   

// INDEX
router.get('/:yearId', (req,res,next) => {
    Record.find({yearId: req.params.yearId})
    .then(records => {
        records.sort(function(a, b) {
            var prev = a.name.toLowerCase();
            var next = b.name.toLowerCase();
            if (prev < next) {
              return -1;
            }
            if (prev > next) {
              return 1;
            }
          
            // names must be equal
            return 0;
          });
        res.json(records)
    });
})



module.exports = router;