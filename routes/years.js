const express = require('express');
const Year = require('./../models/Years')

const router = express.Router();

// GET
router.get('/', (req,res,next) => {
    Year.find()
    .then(years => {
        res.json(years)
    })
    .catch(next)
})

module.exports = router;