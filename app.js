const express = require ('express');
const bodyParser = require ('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')
require('dotenv').config()

const app = express();
app.use(cors());

const port = process.env.PORT || 3001;

mongoose.connect(
    process.env.DB_HOST,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
})
.then(console.log("Database connected"));

app.use(bodyParser.json());

app.use('/records', require('./routes/records'));
app.use('/years', require('./routes/years'));

app.listen(port, () => console.log(`Listening to port ${port}`));