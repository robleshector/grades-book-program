const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RecordSchema = new Schema ({
    name : {
        type: String,
        required: true
    },
    yearId : {
        type: String,
        required: true
    },
    quarterGrades : [],
    finalGrade : Number
});

module.exports = mongoose.model('Record', RecordSchema);